package com.hfad.workout;

import android.os.Bundle;

import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.content.Context;
import android.widget.ListView;

public class WorkoutListFragment extends ListFragment {
    // add listener to the fragment
    static interface Listener {
        void itemClicked(long id);
    };
    private Listener listener;

    //This is called when the fragment gets attached to the activity.
    // activity is subclass of context
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (Listener)context;
    }

    //tell listener when an item in the ListView is clicked.
    @Override
    public void onListItemClick(ListView listView, View itemView, int position, long id) {
        if (listener != null) {
            listener.itemClicked(id);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String[] names =  new String[Workout.workouts.length];
        for (int i = 0; i < names.length; i++) {
            names[i] = Workout.workouts[i].getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                inflater.getContext(), android.R.layout.simple_list_item_1,names);
        setListAdapter(adapter);//blind the array adapter to list view
        // gives you the default layout for the ListFragment.
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}

