package com.example.toplevelactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView;
import android.content.Intent;
import android.os.Bundle;

public class DrinkCategoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_category);
        // create array adapter
        ArrayAdapter<Drink> listAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                Drink.drinks);
        // attach the array adapter to list view
        ListView listDrinks = (ListView) findViewById(R.id.list_drinks);
        listDrinks.setAdapter(listAdapter);

        // create the listener
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?>listDrinks,
                                       View itemView,
                                        int position,
                                       long id) {
                    // passs its id in the list view is clicked
                    Intent intent = new Intent(DrinkCategoryActivity.this, coffee.class);
                    intent.putExtra(coffee.EXTRA_DRINKID, (int) id);
                    startActivity(intent);
                }
            };
            //Assign the listener to the list view
            listDrinks.setOnItemClickListener(itemClickListener);
        }
}