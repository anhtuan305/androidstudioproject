package com.example.toplevelactivity;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ListView;
import android.view.View;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //create an OnItemClickListener
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> ListView,
                                    View  itemView, int posion,
                                    long id) {
                //Drinks is the first item in the list view, so it’s at position 0.
                if(posion == 0) {
                    Intent intent = new Intent(MainActivity.this, DrinkCategoryActivity.class);
                    startActivity(intent);
                }
            }
        };
        //Add the listener to the list view to respond to click
        ListView listview = (ListView) findViewById(R.id.list_option);
        listview.setOnItemClickListener(itemClickListener);
    }
}